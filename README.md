# Programación Logica y Funcional
## Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
  .yellow {
    BackgroundColor yellow
  }
  .orange {
    BackgroundColor orange
  }
  .cyan {
    BackgroundColor cyan
  }
  .lime {
    BackgroundColor lime
  }
  .coral {
    BackgroundColor coral
  }
  .red {
    BackgroundColor red
  }
  .gold {
    BackgroundColor gold
  }
  .silver {
    BackgroundColor silver
  }
  .pink {
    BackgroundColor pink
  }
  .purple {
    BackgroundColor purple
  }
}
</style>
* Programación Declarativa. Orientaciones y pautas para el estudio
** Antecedentes <<green>>
***_ el
**** Instrumento principal <<rose>>
*****_ de
****** Programación <<blue>>
*******_ es la
******** Asignación <<yellow>>
*********_ que
********** Modifica <<orange>>
***********_ el
************ Estado de la memoria <<cyan>>
*************_ lo que
************** Obliga <<lime>>
***************_ al
**************** Programador <<coral>>
*****************_ a dar un
****************** Exceso <<red>>
*******************_ de 
******************** Detalles <<gold>>
** Definición <<green>>
***_ llamada
**** Programación declarativa, perezosa o cómoda <<rose>>
*****_ hace enfasis en el
****** Aspecto creativo <<blue>>
*******_ de la
******** Programación <<yellow>>
*********_ dejando el
********** Aspecto burocratico <<orange>>
***********_ es decir, la
************ Gestión de memoria <<cyan>>
*************_ pero aun
************** Requiere <<lime>>
***************_ que la
**************** Mente del programador <<coral>>
*****************_ considere la
****************** Correspondencia <<red>>
*******************_ entre los
******************** Algoritmos <<gold>>
*********************_ y la 
********************** Máquina <<silver>>
*************_ sus
************** Ventajas <<lime>>
***************_ incluyen
**************** Menos tiempo de ejecución <<coral>>
*****************_ y
****************** Menor tiempo para modificarse <<red>>
** Variantes <<green>>
***_ aunque se llame
**** Programación Declarativa <<rose>>
*****_ el
****** Lenguaje <<blue>>
*******_ sigue siendo
******** Imperativo <<yellow>>
*********_ tratando al 
********** Ordenador <<orange>>
***********_ como una
************ Máquina tonta <<cyan>>
*************_ y tiene
************** Subdivisiones <<lime>>
*************** Programación Funcional <<coral>>
****************_ trata los
***************** Programas <<red>>
******************_ como
******************* Funciones matemáticas <<gold>>
********************_ donde los
********************* Datos de entrada <<silver>>
**********************_ solo se
*********************** Ejecutan <<pink>>
************************_ cuando hay
************************* Consultas <<purple>>
*************** Programación Lógica <<coral>>
****************_ aqui los
***************** Datos de entrada <<red>>
******************_ y de
******************* Salida <<gold>>
********************_ no se
********************* Diferencian <<silver>>
****************_ que usa
***************** Axiomas <<red>>
******************_ y
******************* Reglas de inferencia <<gold>>
********************_ para que el
********************* Compilador <<silver>>
**********************_ dé una
*********************** Respuesta <<pink>>
** Programación General <<green>>
***_ consiste de
**** Dos partes <<rose>>
***** Parte Creativa <<blue>>
******_ donde
******* Pensamos <<yellow>>
********_ en la 
********* Solución <<orange>>
**********_ a la
*********** Problemática
***** Parte Burocratica <<blue>>
******_ donde se hace la
******* Gestión detallada <<yellow>>
********_ de la
********* Memoria <<orange>>
**********_ para seguir una
*********** Secuencia de ordenes <<cyan>>
@endmindmap
```
## Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
  .yellow {
    BackgroundColor yellow
  }
  .orange {
    BackgroundColor orange
  }
  .cyan {
    BackgroundColor cyan
  }
  .lime {
    BackgroundColor lime
  }
  .coral {
    BackgroundColor coral
  }
  .red {
    BackgroundColor red
  }
  .gold {
    BackgroundColor gold
  }
  .silver {
    BackgroundColor silver
  }
  .pink {
    BackgroundColor pink
  }
  .purple {
    BackgroundColor purple
  }
}
</style>
* Lenguaje de Programación Funcional
** Paradigmas de programación <<green>>
***_ son el
**** Modelo de computación <<green>>
*****_ en el que los
****** Diferentes lenguajes <<green>>
*******_ dotan de
******** Semántica <<green>>
*********_ a los
********** Programas <<green>>
** Inicios <<rose>>
***_ los
**** Lenguajes <<rose>>
*****_ eran
****** Basados <<rose>>
*******_ en el
******** Modelo de Von Neumann <<rose>>
*********_ también denominado
********** Imperativo <<lime>>
***********_ dado que se dan
************ Ordenes <<lime>>
*************_ a la
************** Máquina <<lime>>
*********_ donde se
********** Propuso <<blue>>
***********_ que los
************ Programas <<blue>>
*************_ debian
************** Almacenarse <<blue>>
***************_ en la misma
**************** Máquina <<blue>>
*********_ la
********** Ejecución <<yellow>>
***********_ consistia de una
************ Serie <<yellow>>
*************_ de
************** Instrucciones ejecutadas secuencialmente <<yellow>>
*********_ una de sus
********** Caracteristicas <<orange>>
***********_ es la 
************ Instrucción de asignación <<orange>>
*************_ donde las
************** Variables <<orange>>
***************_ pueden ser
**************** Modificadas <<orange>>
*****************_ según la
****************** Zona de memoria <<orange>>
*****************_ durante la
****************** Ejecución <<orange>>
** Paradigma imperativo <<cyan>>
***_ donde un
**** Programa <<cyan>>
*****_ es una
****** Colección de datos <<cyan>>
*******_ y una
******** Serie de instrucciones <<cyan>>
*********_ que
********** Operan <<cyan>>
***********_ sobre esos
************ Datos <<cyan>>
*************_ y la
************** Ejecución <<cyan>>
***************_ de dichas
**************** Instrucciones <<cyan>>
*****************_ en determinado 
****************** Orden <<cyan>>
***_ usan lo 
**** Conocido <<cyan>>
*****_ como
****** Evaluación impaciente <<cyan>>
*******_ o
******** Evaluación estricta <<cyan>>
*********_ y algunos la
********** Evaluación en corto circuito <<cyan>>
** Computación <<coral>>
***_ puede
**** Definirse <<coral>>
*****_ basandose en
****** Lógica Simbolica <<coral>>
*******_ llegando a un 
******** Paradigma <<coral>>
*********_ de 
********** Programación Lógica <<coral>>
***********_ donde un
************ Programa <<coral>>
*************_ se forma por un
************** Conjunto de sentencias <<coral>>
***************_ que definen la
**************** Verdad <<coral>>
*****************_ con respecto a un
****************** Problema <<coral>>
***********_ donde no se
************ Necesita <<coral>>
*************_ que el
************** Programa <<coral>>
**************_ sepa la
*************** Secuencia de pasos <<coral>>
****************_ solamente los
***************** Datos <<coral>>
****** Lambda cálculo <<red>>
*******_ por
******** Church Kleene <<red>>
*********_ inicialmente pensado para
********** Estudiar <<red>>
***********_ el concepto de
************ Función <<red>>
*************_ pero se obtuvo un 
************** Sistema computacional <<red>>
***************_ equivalente al de
**************** Von Neumann <<red>>
*****************_ probando que los
****************** Modelos computacionales <<red>>
*******************_ son
******************** Equivalentes <<red>>
*******_ estudiado por
******** Haskell Curry <<gold>>
*********_ creando una
********** Variante <<gold>>
***********_ llamada
************ Lógica combinatoria <<gold>>
*************_ que fue el
************** Cimiento <<gold>>
***************_ para la
**************** Programación Funcional <<gold>>
** Programación Funcional <<silver>>
***_ nos encontramos con
**** Funciones <<silver>>
*****_ en su
****** Concepción Matemática <<silver>>
*******_ que reciben un
******** Parametro de entrada <<silver>>
*********_ y devuelven un
********** Parametro de salida <<silver>>
***********_ pero pierde el concepto de
************ Asignación <<silver>>
*************_ y a su vez se pierde de 
************** Bucles <<silver>>
************** Factoriales <<silver>>
***_ su mayor
**** Ventaja <<silver>>
*****_ es que la
****** Ausencia <<silver>>
*******_ de 
******** Variables <<silver>>
*********_ permite que los
********** Programas <<silver>>
***********_ solo dependan de
************ Datos de entrada <<silver>>
*************_ y el 
************** Orden <<silver>>
***************_ de los
**************** Cálculos <<silver>>
*****************_ es
****************** Indistinto <<silver>>
*********_ haciendo que en un
********** Entorno Multiprocesado <<silver>>
***********_ las
************ Funciones <<silver>>
*************_ no son
************** Coodependendientes <<silver>>
***************_ permitiendo las
**************** Interrupciones <<silver>>
*****************_ sin
****************** Problema <<silver>>
** Memoización <<pink>>
***_ acuñada por
**** Donald Michie <<pink>>
*****_ en
****** 1968 <<pink>>
*******_ como una
******** Técnica <<pink>>
*********_ para
********** Acelerar <<pink>>
***********_ los
************ Tiempos de cálculo <<pink>>
*************_ al 
************** Almacenar <<pink>>
***************_ el
**************** Valor de una expresión <<pink>>
*****************_ cuya
****************** Evaluación <<pink>>
*******************_ ya fue
******************** Realizada <<pink>>
*********************_ lo que se 
********************** Conoce <<pink>>
***********************_ como
************************ Paso por necesidad <<pink>>
@endmindmap
```
